package cat.itb.luissierras7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DivisibleCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int fi = scanner.nextInt();
        List<Integer> enters = listarEnters(fi, scanner);
        printDivisibles(enters, fi);
    }

    private static void printDivisibles(List<Integer> enters, int fi) {
        int divisibles=0;
        for (int i = 1; i <= fi; i++) {
            for (Integer e:enters){
                if(e%i==0){
                    divisibles++;
                }
            }
            System.out.printf(" %d: %d %n", i, divisibles);
            divisibles=0;
        }
    }

    private static List<Integer> listarEnters(int fi, Scanner scanner) {
        List<Integer> enters = new ArrayList<>();
        int enter = scanner.nextInt();
        while (enter!=-1){
            enters.add(enter);
            enter= scanner.nextInt();
        }
        return enters;
    }
}

package cat.itb.luissierras7e4.dam.m03.extraordinaria.uf5;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IsValidPlateNumber {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        List<String> matriculas = addMatriculas(scanner);
        String regexCurs = "([0-9]{4})[\\S\\s]+([BCDFGHJKLMNPRSTVWXYZ]{3})";
        Pattern pattern = Pattern.compile(regexCurs);

        for(String matricula:matriculas){
            Matcher matcher = pattern.matcher(matricula);
            if (!matcher.find()){
                System.out.printf("la matricula %s es falsa %n", matricula);
            }
        }
    }

    private static List<String> addMatriculas(Scanner scanner) {
        List<String> matriculas = new ArrayList<>();
        int v = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < v; i++) {
            String matricula= scanner.nextLine();
            matriculas.add(matricula);
        }
        return matriculas;
    }
}

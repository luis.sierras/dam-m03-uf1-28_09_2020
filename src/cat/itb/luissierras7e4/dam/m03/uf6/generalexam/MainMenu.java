package cat.itb.luissierras7e4.dam.m03.uf6.generalexam;


import java.sql.SQLException;
import java.util.Scanner;

public class MainMenu {
    Scanner scanner;
    BooksDAO booksDAO;

    public MainMenu(Scanner scanner, BooksDAO booksDAO) {
        this.scanner = scanner;
        this.booksDAO = booksDAO;
    }

    public void draw() throws SQLException {
        while (true) {
            System.out.printf("%nMenu Principal%n");
            System.out.printf("0.Exit%n1.ListBooksByYearApp%n2.InsertBookApp%n3.LongestBookApp%n");
            int operation = scanner.nextInt();
            switch (operation) {
                case 1:
                    System.out.println("Select per any");
                    selectAny();
                    break;
                case 2:
                    System.out.println("Insertar a la base");
                    insert();
                    break;
                case 3:
                    System.out.println("Llibre més llarg");
                    booksDAO.LongestBook().forEach(System.out::println);
                    break;
                case 0: //exit
                    return;
            }
        }
    }

    private void selectAny() throws SQLException {
        System.out.printf("%nInserta any%n");
        int any = scanner.nextInt();
        System.out.printf("## Books For Year %d %n", any);
        booksDAO.listByYear(any).forEach(System.out::println);
    }

    private void insert() throws SQLException {
        scanner.nextLine();
        System.out.println("Titul");
        String title = scanner.nextLine();
        System.out.println("Autor");
        String author = scanner.nextLine();
        System.out.println("ISBN llibre");
        String isbn = scanner.nextLine();
        System.out.println("Any publicació");
        int any = scanner.nextInt();
        System.out.println("Numero de pagines");
        int numPag = scanner.nextInt();

        booksDAO.insert(new Books(title, author, isbn, any, numPag));
    }

}
//comentario para hacer commit y añadir tag
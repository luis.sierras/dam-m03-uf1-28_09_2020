package cat.itb.luissierras7e4.dam.m03.uf6.generalexam;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BooksDAO {
    Database database;

    public BooksDAO(Database database) {
        this.database = database;
    }

    public Connection getConnection() {
        return database.getConnection();
    }

    private List<Books> resultatList(ResultSet resultat) throws SQLException {
        List<Books> list = new ArrayList<>();
        while (resultat.next()) {
            String titol = resultat.getString("title");
            String autor = resultat.getString("author");
            String ISBN = resultat.getString("isbn");
            int numPag = resultat.getInt("pages");
            int any = resultat.getInt("year");
            list.add(new Books(titol, autor, ISBN, numPag, any));
        }
        return list;
    }

    public List<Books> list() throws SQLException {
        String query = "SELECT * FROM book";
        Statement listStatement = getConnection().createStatement();
        ResultSet resultat = listStatement.executeQuery(query);

        return resultatList(resultat);
    }

    public List<Books> listByYear(int userYear) throws SQLException {
        String query = "SELECT * FROM book WHERE year = '" + userYear + "'";
        Statement listStatement = getConnection().createStatement();
        ResultSet resultat = listStatement.executeQuery(query);

        return resultatList(resultat);
    }

    public List<Books> LongestBook() throws SQLException {
        String query = "SELECT * FROM book where pages = (select max(pages) from book)";
        Statement listStatement = getConnection().createStatement();
        ResultSet resultat = listStatement.executeQuery(query);

        return resultatList(resultat);
    }

    public void insert(Books books) throws SQLException {
        String query = "INSERT INTO book(title, author,isbn, year, pages)" +
                "VALUES(?, ?, ?, ?, ?)";
        PreparedStatement insertStatement = getConnection().prepareStatement(query);
        insertStatement.setString(1, books.getTitol());
        insertStatement.setString(2, books.getAutor());
        insertStatement.setString(3, books.getISBN());
        insertStatement.setInt(4, books.getAny());
        insertStatement.setInt(5, books.getNumPag());

        insertStatement.execute();
        System.out.println("## Book inserted");
    }
}
//comentario para hacer commit y añadir tag
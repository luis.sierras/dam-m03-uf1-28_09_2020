package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class ChooseYourOwnAdventure {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Et despertes a una illa de nit. L'últim que recordes és ofegar-te.");

        System.out.println("que vols fer?");
        System.out.println("1. Espero");
        System.out.println("2. Vaig a cercar menjar");
        int opcion = scanner.nextInt();

        if (opcion == 1) {
            esperarDia();
            if (opcion==1){
                muertePrematura();
            } else {
                palmera();
                if (opcion==1){
                    muertePrematura();
                }else{
                    System.out.println("Desde dalt de la palmera veus un vaixell. Estàs salvat!");
                }
            }
        } else {
            muertePrematura();
        }

    }

    private static void esperarDia() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("S'ha fet de dia. Tens molta gana");
        System.out.println("que vols fer?");
        System.out.println("1. Espero");
        System.out.println("2. Vaig a cercar menjar");
        int opcion = scanner.nextInt();
        }

    private static void palmera () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hi ha una palmera amb cocos.");
        System.out.println("que vols fer?");
        System.out.println("1. No hi pujo, és perillós");
        System.out.println("2. Hi pujo");
        int opcion = scanner.nextInt();
    }

    private static void muertePrematura () {
        System.out.println("Moriste antes de tiempo.");
    }
}


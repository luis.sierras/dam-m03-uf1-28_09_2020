package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class CalculateMyWaterBill {
    public static void main(String[] args) {
        //L'usuari introdueix la lletra del tipus d'habitatge i número de m^3 d'aigua gastats.
        Scanner scanner = new Scanner(System.in);
        System.out.println("indica m³ de agua gastados.");
        double aigua= scanner.nextDouble();
        System.out.println("Letra tipo vivienda: a, b, c, d, e, f, g, h, i.");
        String habitatge = scanner.next();

        if (habitatge.equals("a")) {
            double total = (aigua * 0.25 + 2.46);
            System.out.println(total+"€");

        }else if (habitatge.equals("b")) {
            double total = (aigua * 0.33 + 6.40);
            System.out.println(total + "€");

        }else if (habitatge.equals("c")) {
            double total = (aigua * 0.40 + 7.25);
            System.out.println(total + "€");

        }else if (habitatge.equals("d")) {
            double total = (aigua * 0.50 + 11.21);
            System.out.println(total + "€");

        }else if (habitatge.equals("e")) {
            double total = (aigua * 0.63 + 12.10);
            System.out.println(total + "€");

        }else if (habitatge.equals("f")) {
            double total = (aigua * 1 + 17.28);
            System.out.println(total + "€");

        }else if (habitatge.equals("g")) {
            double total = (aigua * 1.60 + 28.01);
            System.out.println(total + "€");

        }else if (habitatge.equals("h")) {
            double total = (aigua * 2.50 + 40.50);
            System.out.println(total + "€");

        }else if (habitatge.equals("i")) {
            double total = (aigua * 4 + 61.31);
            System.out.println(total + "€");

        }else{
            System.out.println("Tipo de vivienda no encontrado vuelva a intentarlo.");
        }
    }
}

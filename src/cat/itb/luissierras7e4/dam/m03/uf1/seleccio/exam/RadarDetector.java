package cat.itb.luissierras7e4.dam.m03.uf1.seleccio.exam;

import java.util.Scanner;


public class RadarDetector {
    public static void main(String[] args) {
        //Demana una velocitat a l'usuari.
        System.out.println("introduce la velocidad");
        Scanner scanner=new Scanner(System.in);
        double velocitat = scanner.nextDouble();

        //Imprimeix per pantalla el tipus de multa (Correcte, Multa lleu o Multa greu).
        if (velocitat>=120&&velocitat<140){
            System.out.println("Has excedit la velocitat maxima, tens una Multa lleu");
        }else if (velocitat>=140){
            System.out.println("Has excedit la velocitat maxima per molt, tens una Multa Greu");
        }else {
            System.out.println("Continua circulant");
        }
    }
}

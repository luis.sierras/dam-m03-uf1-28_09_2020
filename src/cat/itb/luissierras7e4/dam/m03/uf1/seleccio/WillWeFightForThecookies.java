package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class WillWeFightForThecookies {
    public static void main(String[] args) {
        /*Introdueix el número de persones i el número de galetes.*/
        Scanner numero=new Scanner(System.in);
        System.out.println("personas");
        int personas = numero.nextInt();
        System.out.println("galletas");
        int galletas = numero.nextInt();

        /*Si a tothom li toquen el mateix número de galetes imprimeix "Let's Eat!", sinó imprimeix "Let's Fight"*/
        double division = galletas%personas;
        if (division==0){
            System.out.println("Let's Eat!");
        }else{
            System.out.println("Let's Fight");
        }
    }
}

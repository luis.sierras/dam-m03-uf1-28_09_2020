package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class WhichBigger {
    public static void main(String[] args) {
        /*pedir 2 enteros*/
        Scanner entero = new Scanner(System.in);
        int valor1= entero.nextInt();
        int valor2= entero.nextInt();

        /*imprimir el más grande*/
        if (valor1 > valor2){
            System.out.println(valor1);
        }else {
            System.out.println(valor2);
        }

    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        /*L'usuari introdueix dos enters (1)pedra, (2) paper, (3) tisora.*/
        System.out.println("introduce numero: (1)pedra, (2) paper, (3) tisora");
        Scanner scanner = new Scanner(System.in);
        int player1= scanner.nextInt();
        int player2= scanner.nextInt();

        boolean empate = player1 == player2;

        if (empate){
            goToTie();
        } else if (player1==1 && player2==3 || player1==2 && player2==1 || player1==3 && player2==2){
            player1_win();
        }else{
            player2_win();
        }
    }

    private static void player2_win() {
        System.out.println("Has ganado player2");
    }

    private static void player1_win() {
        System.out.println("Has ganado player1");
    }

    private static void goToTie() {
        System.out.println("habeis empatado");
    }

}

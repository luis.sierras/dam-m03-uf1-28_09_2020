package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArrayMaxValue {
    public static void main(String[] args) {
//L'usuari introduirà 1 array d'enters, com s'indica al mètode ArrayReader.
//Un cop llegits tots, printa per pantalla el valor més gran.
        Scanner scanner = new Scanner(System.in);
        int[] values = ArrayReader.scannerReadIntArray(scanner);
        int maxValue = values[0];
        for(int value : values){
            if(value>maxValue)
                maxValue = value;
        }
        System.out.println(maxValue);
    }
}


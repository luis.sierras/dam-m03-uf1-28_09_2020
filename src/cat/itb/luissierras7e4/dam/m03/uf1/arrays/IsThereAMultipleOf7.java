package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

public class IsThereAMultipleOf7 {
    public static void main(String[] args) {
//Donat el següent vector, imprimeix true si algún dels números és divisible entre 7 o false sinó.
        int[] values = {4,8,9,40,54,84,40,6,84,1,1,68,84,68,4,840,684,25,40,98,54,687,31,4894,468,46,84687,894,40,846,1681,618,161,846,84687,6,848};
        boolean divisible = false;
        for(int i=0;i< values.length && !divisible;++i){
            int result =(values[i]%7);
            if (result == 0) {
                divisible = true;
                break;
            }
        }
        System.out.println(divisible);
    }
}

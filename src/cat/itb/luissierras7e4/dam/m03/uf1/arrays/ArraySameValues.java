package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArraySameValues {
    public static void main(String[] args) {
        //L'usuari introduirà 2 llistes de valors, com s'indica al mètode ArrayReader.
        //Printa per pantalla són iguals si ha introduit la mateixa llista, o no són iguals si són diferents
        Scanner scanner=new Scanner(System.in);
        int[] valores= ArrayReader.scannerReadIntArray(scanner);
        int[] valores2= ArrayReader.scannerReadIntArray(scanner);
        boolean igual=true;
        if (valores.length == valores2.length) {
            for (int i = 0; i < valores2.length; ++i) {
                if (valores[i] != valores2[i]) {
                    igual = !igual;
                }
            }
        }else {
            igual=!igual;
        }
        if (igual){
            System.out.println("són iguals");
        }else{
            System.out.println("no són iguals");
        }
    }
}

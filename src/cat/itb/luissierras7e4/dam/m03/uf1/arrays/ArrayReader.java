package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArrayReader {
    /**
     * Reads an int of arrays from user input.
     * The user first introduces the number (N).
     * Later it introduces the integers one by one.
     * @return int array of values introduced of size N
     */
    public static int[] scannerReadIntArray(Scanner scanner) {
        int total= scanner.nextInt();
        int[] enters = new int[total];
        for(int i=0; i<total; ++i){
            enters[i] = scanner.nextInt();
        }
        return enters;
    }
}

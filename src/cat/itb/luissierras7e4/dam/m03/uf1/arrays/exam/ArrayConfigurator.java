package cat.itb.luissierras7e4.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayConfigurator {
    public static void main(String[] args) {
        //Crea un array de 10 enters inicialitzats amb el valor 0.
        // L'usuari introduirà parelles de valors. La primera indicarà la posició de l'array a modificar i la segona el valor a posar-hi.
        // Quan introdueixi la posició -1 és que s'ha d'acabar el programa i printar l'array en l'estat que ha quedat.
        Scanner scanner=new Scanner(System.in);
        int[] lista= new int[10];
        int posicio= scanner.nextInt();
        int valor= scanner.nextInt();
//TODO SE PUEDE MEJORAR

        while (posicio!=-1){
            lista[posicio]= valor;
            posicio= scanner.nextInt();
            if(posicio==-1){
                break;
            }
            valor= scanner.nextInt();
        }
        System.out.println(Arrays.toString(lista));
    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class MinOf10Values {
    public static void main(String[] args) {
        //L'usuari entra 10 enters. Crea un array amb aquest valors.
        Scanner scanner = new Scanner(System.in);
        int[] enters = new int[10];
        for(int i=0; i<10; ++i){
            int valors = scanner.nextInt();
            enters[i] = valors;
        }

        // Imprimeix per pantalla el valor més petit introduit.
        int newValor= enters[0];
        for(int i=0; i<10; ++i){
            if (enters[i]<newValor){
                newValor = enters[i];
            }
        }
        System.out.println(newValor);
    }
}

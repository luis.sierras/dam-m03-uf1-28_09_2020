package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class CapICuaValues {
    public static void main(String[] args) {
        //Printa per pantalla cap i cua si la llista de N valors introduits per l'usuari són cap i cua (llegits en ordre invers és la mateixa llista).
        Scanner scanner = new Scanner(System.in);
        int[] cap = ArrayReader.scannerReadIntArray(scanner);
        boolean nocap = true;
        for (int i=0; i<(cap.length/2); ++i){
            if(cap[i]!=cap[(cap.length-1)-i]){
                nocap= !nocap;
                break;
            }
        }
        if (nocap){
            System.out.println("cap i cua");
        }
    }
}

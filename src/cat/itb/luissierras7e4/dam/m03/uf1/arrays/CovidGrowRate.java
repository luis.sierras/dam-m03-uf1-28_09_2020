package cat.itb.luissierras7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class CovidGrowRate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] infeccio=ArrayReader.scannerReadIntArray(scanner);
        for (int i=1; i<infeccio.length; ++i){
            double actual= infeccio[i];
            double anterior= infeccio[i-1];
            double resultado=actual/anterior;
            System.out.println(resultado);
        }
    }
}

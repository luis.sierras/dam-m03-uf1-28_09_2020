package cat.itb.luissierras7e4.dam.m03.uf1.arrays.exam;

import java.util.ArrayList;
import java.util.List;

public class HowManyMountains {
    public static void main(String[] args) {
//Usant les imatges d'un satel·lit hem pogut fer raster o (mapa de bits)[https://ca.wikipedia.org/wiki/Mapa_de_bits] que ens indica l'alçada d'un punt concret d'un mapa.
//Imprimeix per pantalla el numero posicions cims que hi ha. Entenrem que un cim ho és si l'alçada de la seva posició és més alta o igual a les posicions col·lidants (no conten les diagonals).
//No contarem com a cim les posicions que estan als marges del mapa.
        double[][] map ={
                {1.5,1.6,1.8,1.7,1.6},
                {1.5,2.6,2.8,2.7,1.6},
                {1.5,4.6,4.4,4.9,1.6},
                {2.5,1.6,3.8,7.7,3.6},
                {1.5,2.6,3.8,2.7,1.6}};
        int cims=0;
//F LUIS TODO CORREGIR
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map.length; j++) {
                if(i==0 && j<4){
                    if (map[i][j] >= map[i][j+1]){
                        cims+=1;
                    }
                }

            }
        }
        System.out.println(cims);
    }
}

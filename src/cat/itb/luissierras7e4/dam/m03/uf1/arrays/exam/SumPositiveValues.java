package cat.itb.luissierras7e4.dam.m03.uf1.arrays.exam;

import cat.itb.luissierras7e4.dam.m03.uf1.arrays.ArrayReader;

import java.util.Scanner;

public class SumPositiveValues {
    public static void main(String[] args) {
//L'usuari introduirà 1 array d'enters, com s'indica al mètode ArrayReader.
//Un cop llegits tots, printa per pantalla la suma de tots els valors positius.
        Scanner scanner=new Scanner(System.in);
        int[] lista = ArrayReader.scannerReadIntArray(scanner);
        int suma= 0;
//TODO SE PUEDE MEJORAR
        for (int i = 0; i < lista.length; i++) {
            if (lista[i]>=0) {
                suma += lista[i];
            }
        }
        System.out.println(suma);
    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.mix;

import java.util.Scanner;

public class RadarFilter {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int count=0;
        int velocitat= scanner.nextInt();
        while (velocitat!=-1){
            if (velocitat>90){
                ++count;
            }
            velocitat= scanner.nextInt();
        }
        System.out.println(count);
    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class HiddenNumber {
    public static void main(String[] args) {
        //L'oridador pensa un número del 1 al 3.
        int ordinador = (int) (Math.random()*3+1);
        //System.out.println(ordinador); comprobar si solo sale 1

        //L'usuari introdueix un enter.
        System.out.println("Introduce entero entre 1 y 3");
        Scanner scanner=new Scanner(System.in);
        int usuari= scanner.nextInt();

        //Imprimeix L'has encertat, o No l'has encertat segons el resultat.
        if (ordinador==usuari){
            System.out.println("L'has encertat");
        } else {
            System.out.println("No l'has encertat");
        }
    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class PowerOf {
    public static void main(String[] args) {
        //demana dos enters (a i b) a l'usuari i imprimeix el valor de a^b
        System.out.println("introdece dos enteros");
        Scanner scanner=new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        int potencia= (int) Math.pow(a,b);
        System.out.println(potencia);
    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

import static cat.itb.luissierras7e4.dam.m03.uf1.staticfunctions.HowBigIsMyPizzaFunc.areaPizzaConDiametro;
import static cat.itb.luissierras7e4.dam.m03.uf1.staticfunctions.IntRectangleAreaFunc.areaRect;

public class WhichPizzaShouldIBuyFunc {
    public static void main(String[] args) {
        /*Volem comparar quina pizza és més gran, entre una rectangular i una rodona*/
        /*L'usuai entra el diametre d'una pizza rodona*/
        Scanner scanner = new Scanner(System.in);
        System.out.println("esee diametro");
        double diametro = scanner.nextDouble();

        double areaRodona = areaPizzaConDiametro(diametro);

        /*L'usuari entra els dos costats de la pizza rectangular*/
        System.out.println("esa base y altura loko");
        double base = scanner.nextDouble();
        double altura = scanner.nextDouble();

        double areaRectangle = areaRect(base, altura);

        /*Imprimeix "Compra la rodona" si la pizza rodona és més gran, o "Compta la rectangular" en qualsevol altre cas.*/
        if (areaRodona > areaRectangle) {
            System.out.println("Compra la rodona");
        } else {
            System.out.println("Compra la rectangular");
        }
    }
}

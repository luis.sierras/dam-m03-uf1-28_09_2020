package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.ArrayList;
import java.util.Scanner;

public class OnlyVowels {
    public static void main(String[] args) {
        //Donada una llista de lletres, imprimeix únicament les vocals que hi hagi.
        //L'entrada consta de dues parts:

        //Primer s'indica la quantitat de lletres
        Scanner scanner=new Scanner(System.in);
        int numero= scanner.nextInt();

        //A continuació venen les lletres separades per espais en blanc
        ArrayList<String> vocals= new ArrayList<>();
        for(int i=0; i<numero; i++){
            String lletres= scanner.next();
            if(lletres.equals("a")||lletres.equals("e")||lletres.equals("i")||lletres.equals("o")||lletres.equals("u")) {
                vocals.add(lletres);
            }
        }
        for(int i=0; i<numero; i++) {
            System.out.println(vocals.get(i));
        }
    }
}

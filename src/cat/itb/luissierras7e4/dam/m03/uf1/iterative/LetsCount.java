package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class LetsCount {
    public static void main(String[] args) {
        //Printa per pantalla tots els números fins a un enter entrat per l'usuari
        Scanner scanner=new Scanner(System.in);
        int numero = scanner.nextInt();
        int i = 1;
        while (i<=numero) {
            System.out.print(i);
            i++;
        }
    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class CountWithJumps {
    public static void main(String[] args) {
        //L'usuari introdueix dos valors enters, el final i el salt
        Scanner scanner = new Scanner(System.in);
        int fin =scanner.nextInt();
        int salt= scanner.nextInt();

        //Escriu tots els numeros des de l'1 fins al final, amb una distància de salt
        for(int i=1;i<=fin;i+=salt){
            System.out.print(i+" ");
        }
    }
}

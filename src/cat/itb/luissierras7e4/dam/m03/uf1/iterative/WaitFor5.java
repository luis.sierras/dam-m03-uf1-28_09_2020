package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class WaitFor5 {
    public static void main(String[] args) {
        //L'usuari introdueix enters.
        Scanner scanner = new Scanner(System.in);
        int valor = scanner.nextInt();

        //Quan introdueixi el 5 imprimeix 5 trobat!.
        while (valor!=5) {
            valor = scanner.nextInt();
        }
        System.out.println("5 trobat!");
    }
}

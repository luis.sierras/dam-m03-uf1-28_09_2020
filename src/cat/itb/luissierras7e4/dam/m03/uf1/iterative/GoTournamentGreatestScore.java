package cat.itb.luissierras7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class GoTournamentGreatestScore {
    public static void main(String[] args) {
        //L'usuari introduirà el nom del participant i la puntuació, cada un en una línia.
        Scanner scanner=new Scanner(System.in);
        String nom = scanner.nextLine();
        int punts = scanner.nextInt();
        int bestScore=punts;
        String bestPlayer=nom;

        // Quan ja no hi hagi més participants entrara la paraula clau END.
        while (!nom.equals("END")){
            if(punts>bestScore){
                bestScore=punts;
                bestPlayer=nom;
            }
            nom = scanner.nextLine();
            punts = scanner.nextInt();

        }

        // Imprimeix per pantalla el guanyador del concurs i els punts obtinguts.
        System.out.println(bestPlayer +": " +bestScore);
    }
}

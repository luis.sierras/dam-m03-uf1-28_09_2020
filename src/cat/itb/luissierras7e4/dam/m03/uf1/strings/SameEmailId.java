package cat.itb.luissierras7e4.dam.m03.uf1.strings;

import java.util.Scanner;

public class SameEmailId {
    public static void main(String[] args) {
//En alguns servidors de correu electrònic (gmail per exemple), l'identificador de l'usuari no té en compte les majuscules ni els punts.
// Per tant els següents correus són el mateix.

        Scanner scanner=new Scanner(System.in);
        String correu1= scanner.next();
        String correu2= scanner.next();

        correu1 = correu1.replace(".","");
        correu2 = correu2.replace(".","");

        System.out.println(correu1);
        System.out.println(correu2);


    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.strings.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CristmasGenerator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix els anys:");
        String oldYear = scanner.nextLine();
        String newYear = scanner.nextLine();

        System.out.println("Introdueix els noms a felicitar, l'antic i el nou:");
        String oldName = scanner.nextLine();
        String newName = scanner.nextLine();

        System.out.println("Introdueix els noms dels escriptors:");
        String oldWriter = scanner.nextLine();
        String newWriter = scanner.nextLine();

        System.out.println("Ara introdueix el missatge a modificar (END per acabar):");
        List<String> misatge = new ArrayList<String>();
        String cos = scanner.nextLine();

        while (!cos.equals("END")){
            misatge.add(cos);
            cos = scanner.nextLine();
        }
        for (String s : misatge) {
            System.out.printf("%s\n", (s.replace(oldYear, newYear)
                    .replace(oldName, newName)
                    .replace(oldWriter, newWriter)));
        }

    }
}

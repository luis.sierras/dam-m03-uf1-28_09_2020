package cat.itb.luissierras7e4.dam.m03.uf1.strings.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SuperSecretText {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] lletres = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
        // length 26

        System.out.println("Introdueix 1 per xifrar, 0 per dexifrar:");
        int userInput = scanner.nextInt();
        while (userInput != 1 && userInput != 0){
            System.out.println("El número ha de ser 1 o 0.");
            userInput = scanner.nextInt();
        }
        if (userInput == 1){
            System.out.println("Introdueix l'enter per xifrar:");
            int xifrador = scanner.nextInt();
            xifrar(lletres, xifrador);
        }else if (userInput == 0){
            System.out.println("Introdueix l'enter per dexifrar:");
            int dexifrador = scanner.nextInt();
            dexifrar(lletres, dexifrador);
        }
    }

    private static
    void dexifrar(String[] lletres, int dexifrador) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix el text a dexifrar:");
        String text = scanner.nextLine();

        List<String> newText = new ArrayList<String>();
        for (int i = 0; i < text.length(); ++i){
            String letter = String.valueOf(text.charAt(i));
            int position = 0;
            for (int j = 0; j < lletres.length; ++j){
                if (lletres[j].equals(letter)){
                    position = j;
                    break;
                }
            }
            if ((position - dexifrador) < 0 && !String.valueOf(text.charAt(i)).equals("'")){
                position = (position - dexifrador) + (lletres.length + 1);
                String newLetter = lletres[(position - 1)];
                newText.add(newLetter);
            } else if (String.valueOf(text.charAt(i)).equals("'")){
                newText.add(" ");
            }else{
                String newLetter = lletres[(position - dexifrador)];
                newText.add(newLetter);
            }
        }
        for (String s : newText) {
            System.out.print(s);
        }
    }

    private static void xifrar(String[] lletres, int xifrador) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix el text a xifrar:");
        String text = scanner.nextLine();

        List<String> newText = new ArrayList<String>();
        for (int i = 0; i < text.length(); ++i){
            String letter = String.valueOf(text.charAt(i));
            int position = 0;
            for (int j = 0; j < lletres.length; ++j){
                if (lletres[j].equals(letter)){
                    position = j;
                    break;
                }
            }
            if ((position + xifrador) > (lletres.length - 1) && !String.valueOf(text.charAt(i)).equals(" ")){
                position = (position + xifrador) - (lletres.length - 1);
                String newLetter = lletres[(position - 1)];
                newText.add(newLetter);
            } else if (String.valueOf(text.charAt(i)).equals(" ")){
                newText.add("'");
            }else{
                String newLetter = lletres[(position + xifrador)];
                newText.add(newLetter);
            }
        }
        for (String s : newText) {
            System.out.print(s);
        }
    }

}

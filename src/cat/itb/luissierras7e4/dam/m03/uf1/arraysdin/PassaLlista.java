package cat.itb.luissierras7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class PassaLlista {
    public static void main(String[] args) {
//Volem fer un programa que ens digui quins alumnes no han vingut a classe.
//L'usuari introduirà enters ordenats de major a menor, indicant la posició dels alumnes que han vingut a classe.
//Quan introdueixi un -1 és que ja ha acabat.
        Scanner scanner=new Scanner(System.in);
        List<String> alumne = new ArrayList<String>(Arrays.asList("Magalí", "Magdalena", "Magí", "Manel", "Manela", "Manuel", "Manuela", "Mar", "Marc", "Margalida", "Marçal", "Marcel", "Maria", "Maricel", "Marina", "Marta", "Martí", "Martina"));
        System.out.println("numeros de mayor a menor");
        int valor = scanner.nextInt();
        while(valor!=-1){
            alumne.remove(valor);
            valor = scanner.nextInt();
        }
        System.out.println(alumne);
    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AnotherInverseOrder {
    public static void main(String[] args) {
        //L'usuari entrarà un conjunt d'enters per pantalla. Quan introdueixi el -1, és que ja ha acabat.
        //Imprimeix-los en l'odre invers al que els ha entrat.
        Scanner scanner = new Scanner(System.in);
        int valor=scanner.nextInt();
        List<Integer> enters=new ArrayList<Integer>();

        while(valor!=-1) {
            enters.add(valor);
            valor=scanner.nextInt();
            /* OTRA OPCIÓN
            enters.add(0,valor);
            valor=scanner.nextInt();*/
        }
        for (int i=enters.size()-1;i>=0;--i){
            System.out.println(enters.get(i));
        }
    }
}

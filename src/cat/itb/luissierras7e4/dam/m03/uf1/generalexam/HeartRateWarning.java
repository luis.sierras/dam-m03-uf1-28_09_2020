package cat.itb.luissierras7e4.dam.m03.uf1.generalexam;

import java.util.Scanner;

public class HeartRateWarning {
    public static void main(String[] args) {
//L'empresa de rellotges esportius RunWithMe ens ha demanat que fem un petit mòdul per els seus rellotges intel·ligents per el control del rítme cardíac.
// Volem tenir controlat quan un esportista té el ritme cardíac massa alt o massa baix durant un període de temps elevat de forma consecutiva.
        Scanner scanner=new Scanner(System.in);
        int minimDes=scanner.nextInt();
        int maximDes= scanner.nextInt();
        int ritme=scanner.nextInt();
        int countMax=0;
        int countMin=0;
        while(ritme!=-1){
            if(ritme>maximDes){
                countMax+=1;
                countMin=0;
            }else if(ritme<minimDes){
                countMin+=1;
                countMax=0;
            }else{
                countMax=0;
                countMin=0;
            }

            if(countMax>=3){
                System.out.println("MASSA ALT");
            }else if(countMin>=3){
                System.out.println("MASSA BAIX");
            }

            ritme=scanner.nextInt();
        }
    }
}

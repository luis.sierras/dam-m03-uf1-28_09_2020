package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class OneIs10 {
    public static void main(String[] args) {
        /*L'usuari introdueix 4 enters*/
        Scanner enteros = new Scanner(System.in);
        int entero1 = enteros.nextInt();
        int entero2 = enteros.nextInt();
        int entero3 = enteros.nextInt();
        int entero4 = enteros.nextInt();

        /*Printa true si algun és 10 o false en qualsevol altre cas.*/
        boolean resultado = entero1 == 10 || entero2 == 10 || entero3 == 10 || entero4 == 10;
        System.out.println(resultado);
    }
}

package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class HowBigIsMyPizza {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double diametro = scanner.nextDouble();
        double radio = (diametro / 2.0);
        double superficie = Math.pow(radio,2)* 3.14;
        System.out.println(superficie);
    }
}

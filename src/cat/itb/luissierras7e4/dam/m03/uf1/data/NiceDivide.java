package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class NiceDivide {
    public static void main(String[] args) {
        /* pedir enteros */
        Scanner entero = new Scanner(System.in);
        int valor1 = entero.nextInt();
        int valor2 = entero.nextInt();

        /* cálculos matimáticos super de la nasa*/
        int division = valor1 / valor2;
        int modulo = valor1 % valor2;

        /* impresion */
        System.out.println(valor1 +" dividit entre " +valor2 +" és " +division +" mòdul " +modulo);
    }
}

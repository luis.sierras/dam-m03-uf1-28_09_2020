package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class CalculateDiscount {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
            double original= scanner.nextDouble();
            double descuento= scanner.nextDouble();
            double percentage = (descuento -original)/original*100;

            System.out.println(percentage+"%");
    }

}

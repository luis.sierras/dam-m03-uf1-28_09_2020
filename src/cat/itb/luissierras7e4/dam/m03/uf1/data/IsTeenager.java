package cat.itb.luissierras7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsTeenager {
    public static void main(String[] args) {
        //L'usuari escriu un enter amb la seva edat i s'imprimeix true si l'edat està entre 10 i 20.
        Scanner scanner=new Scanner(System.in);
        int edat= scanner.nextInt();

        boolean teen=(edat>=10&&edat<=20);

        System.out.println(teen);
    }
}

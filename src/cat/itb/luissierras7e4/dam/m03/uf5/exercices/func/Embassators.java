package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

public class Embassators {
    String FirstName;
    String LastName;
    Country Country;


    public Embassators(String FirstName, String LastName, Country Country) {
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Country = Country;
    }


    public String getFirstName(){
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public Country getCountry() {
        return Country;
    }

    public String getCountryName() {
        return Country.getName();
    }
}

package cat.itb.luissierras7e4.dam.m03.uf5.exercices.tads;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class RepeatedAnswer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<String> set = new HashSet<>();

        String input = scanner.nextLine();

        while (!input.equals("END")){
            boolean mec = set.add(input);
            if (!mec){
                System.out.println("MEEEC!");
            }
            input = scanner.nextLine();
        }
    }
}

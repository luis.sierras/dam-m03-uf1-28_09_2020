package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class NearestTo100 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        int result = Collections.min(list, Comparator.comparing(NearestTo100::distanceTo100));
        System.out.println(result);
    }

    public static int distanceTo100(int x){
        return Math.abs(100-x);
    }

}

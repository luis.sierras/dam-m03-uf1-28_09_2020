package cat.itb.luissierras7e4.dam.m03.uf5.exercices.func;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class EmbassatorsByBiggestCountry {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countries = CountryDataSortByName.readCountries(scanner);
        List<Embassators> embassators = readEmbassators(scanner, countries);
        embassators.sort(Comparator.comparing(Embassators::getCountryName)
                .thenComparing(Embassators::getLastName)
                .thenComparing(Embassators::getFirstName));
    }



    private static List<Embassators> readEmbassators(Scanner scanner, List<Country> countries) {
        int count = scanner.nextInt();
        List<Embassators> embassators = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Embassators embassator = readEmbassator(scanner, countries);
            embassators.add(embassator);
        }
        return embassators;
    }

    private static Embassators readEmbassator(Scanner scanner, List<Country> countries) {
        String firstname = scanner.nextLine();
        String lastname = scanner.nextLine();
        String countryName = scanner.nextLine();
        Country country = countries.stream().filter(c -> c.name.equals(countryName)).findFirst().get();
        return new Embassators(firstname, lastname, country);
    }

}

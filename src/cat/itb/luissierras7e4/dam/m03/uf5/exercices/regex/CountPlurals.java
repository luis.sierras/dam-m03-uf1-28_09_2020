package cat.itb.luissierras7e4.dam.m03.uf5.exercices.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountPlurals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String regex = "\\w+s[, \\.]";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);

        int plurals = 0;
        while (matcher.find()){
            ++plurals;
        }

        System.out.println(plurals);

    }
}

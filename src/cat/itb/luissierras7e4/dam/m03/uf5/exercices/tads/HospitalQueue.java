package cat.itb.luissierras7e4.dam.m03.uf5.exercices.tads;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class HospitalQueue {
    public static void main(String[] args) {
        Queue<Patient> queue = new PriorityQueue<>();
        Scanner scanner = new Scanner(System.in);
        int operation = scanner.nextInt();
        while(operation!=-1) {
            switch (operation) {
                case 1:
                    addPatientToQueue(scanner, queue);
                    break;
                case 0:
                    removePatient(queue);
                    break;
            }
            operation = scanner.nextInt();
        }
    }

    private static void removePatient(Queue<Patient> queue) {
        Patient patient = queue.poll();
        System.out.printf("%s passi a consulta%n", patient.getName());
    }

    private static void addPatientToQueue(Scanner scanner, Queue<Patient> queue) {
        int priority = scanner.nextInt();
        String name = scanner.nextLine();
        Patient patient = new Patient(priority, name);
        queue.add(patient);
    }


}

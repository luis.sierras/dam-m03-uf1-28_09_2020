package cat.itb.luissierras7e4.dam.m03.uf5.practica;

public class Data {
    String nameCountry;
    String codeCountry;
    int newCases;
    int totalCases;
    int newDeaths;
    int totalDeaths;
    int newRecovered;
    int totalRecovered;

    public Data(String nameCountry, String codeCountry, int newCases,
                int totalCases, int newDeaths, int totalDeaths, int newRecovered, int totalRecovered) {
        this.nameCountry = nameCountry;
        this.codeCountry = codeCountry;
        this.newCases = newCases;
        this.totalCases = totalCases;
        this.newDeaths = newDeaths;
        this.totalDeaths = totalDeaths;
        this.newRecovered = newRecovered;
        this.totalRecovered = totalRecovered;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public String getCodeCountry() {
        return codeCountry;
    }

    public int getNewCases() {
        return newCases;
    }

    public int getTotalCases() {
        return totalCases;
    }

    public int getNewDeaths() {
        return newDeaths;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public int getNewRecovered() {
        return newRecovered;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }


    public static boolean isNotEuropean(Data data) {
        String[] codes = {"BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI", "DK", "HR",
                "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "PL"};

        for (String code : codes) {
            if (data.getCodeCountry().equals(code)) {
                return false;
            }
        }
        return true;
    }
}


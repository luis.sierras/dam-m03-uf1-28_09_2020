package cat.itb.luissierras7e4.dam.m03.uf5.practica;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Tops {
    //Tops
    static void printTop(List<Data> dades) {
        System.out.printf("### TOPS ###\n" +
                        "Païs amb més casos nous: %s\n" +
                        "Païs amb més casos totals: %s\n" +
                        "Païs amb més morts noves: %s\n" +
                        "Païs amb més morts totals: %s\n" +
                        "Païs amb més recuperats nous: %s\n" +
                        "Païs amb més recuperats totals: %s\n",
                topNewCases(dades), topTotalCases(dades), topNewDeaths(dades),
                topTotalDeaths(dades), topNewRecovered(dades), topTotalRecovered(dades));
    }

    //TOP EU
    static void printTopEU(List<Data> dades) {
        dades.removeIf(Data::isNotEuropean);
        System.out.printf("### Tops EU ###\n" +
                        "Païs amb més casos nous: %s\n" +
                        "Païs amb més casos totals: %s\n" +
                        "Païs amb més morts noves: %s\n" +
                        "Païs amb més morts totals: %s\n" +
                        "Païs amb més recuperats nous: %s\n" +
                        "Païs amb més recuperats totals: %s\n",
                topNewCases(dades), topTotalCases(dades), topNewDeaths(dades),
                topTotalDeaths(dades), topNewRecovered(dades), topTotalRecovered(dades));

    }


    private static Object topNewCases(List<Data> dades) {
        Data dada = Collections.max(dades, Comparator.comparing(Data::getNewCases));
        return dada.getNameCountry();
    }

    private static Object topTotalCases(List<Data> dades) {
        Data dada = Collections.max(dades, Comparator.comparing(Data::getTotalCases));
        return dada.getNameCountry();
    }

    private static Object topNewDeaths(List<Data> dades) {
        Data dada = Collections.max(dades, Comparator.comparing(Data::getNewDeaths));
        return dada.getNameCountry();
    }

    private static Object topTotalDeaths(List<Data> dades) {
        Data dada = Collections.max(dades, Comparator.comparing(Data::getTotalDeaths));
        return dada.getNameCountry();
    }

    private static Object topNewRecovered(List<Data> dades) {
        Data dada = Collections.max(dades, Comparator.comparing(Data::getNewRecovered));
        return dada.getNameCountry();
    }

    private static Object topTotalRecovered(List<Data> dades) {
        Data dada = Collections.max(dades, Comparator.comparing(Data::getTotalRecovered));
        return dada.getNameCountry();
    }
}

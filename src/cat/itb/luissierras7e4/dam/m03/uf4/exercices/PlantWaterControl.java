package cat.itb.luissierras7e4.dam.m03.uf4.exercices;

import java.util.List;

public class PlantWaterControl {
    PlantWater plantWater;

    public PlantWaterControl(PlantWater plantWater) {
        this.plantWater = plantWater;
    }

    public void waterIfNeeded(){
        if(waterNeeded())
            plantWater.startWatterSystem();
    }

    private boolean waterNeeded() {
        List<Double> humidityValues = plantWater.getHumidityRecord();
        double average = calculateAverage(humidityValues);
        return average<2;
    }

    private double calculateAverage(List<Double> humidityValues) {
        return sum(humidityValues)/humidityValues.size();
    }

    private double sum(List<Double> humidityValues) {
        double sum = 0;
        for(double value: humidityValues){
            sum+=value;
        }
        return sum;
    }
}


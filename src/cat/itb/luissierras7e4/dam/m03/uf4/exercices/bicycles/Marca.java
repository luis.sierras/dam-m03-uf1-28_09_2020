package cat.itb.luissierras7e4.dam.m03.uf4.exercices.bicycles;

public class Marca {
    String name;
    String country;

    public Marca(String name, String country) {
        this.name = name;
        this.country = country;
    }

    @Override
    public String toString() {
        return "Marca{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}

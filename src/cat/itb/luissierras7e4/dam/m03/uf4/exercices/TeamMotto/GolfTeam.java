package cat.itb.luissierras7e4.dam.m03.uf4.exercices.TeamMotto;


public class GolfTeam extends Team {
    String player;

    public GolfTeam(String name, String motto, String player) {
        super(name, motto);
        this.player = player;
    }

    @Override
    public void shoutMotto() {
        System.out.println(player + " " + motto);
    }
}


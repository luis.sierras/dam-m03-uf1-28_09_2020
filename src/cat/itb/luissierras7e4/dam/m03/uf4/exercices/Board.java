package cat.itb.luissierras7e4.dam.m03.uf4.exercices;

import cat.itb.luissierras7e4.dam.m03.uf2.classfun.Rectangle;

import java.util.List;

public class Board {
    List<Rectangle> rectangles;

    public Board(List<Rectangle> rectangles) {
        this.rectangles = rectangles;
    }

    public double getTotalArea(){
        double area = 0;
        for(Rectangle rectangle: rectangles){
            area+= rectangle.getArea();
        }
        return area;
    }
    public int countRectangle(){
        return rectangles.size();
    }

}

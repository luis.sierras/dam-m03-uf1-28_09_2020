package cat.itb.luissierras7e4.dam.m03.uf4.exercices.bicycles;

public class ModelPatinet {
    String name;
    int potencia;
    Marca brand;

    public ModelPatinet(String name, int potencia, Marca brand) {
        this.name = name;
        this.potencia = potencia;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Model{" +
                "name='" + name + '\'' +
                ", potencia=" + potencia +
                ", brand=" + brand +
                '}';
    }
}

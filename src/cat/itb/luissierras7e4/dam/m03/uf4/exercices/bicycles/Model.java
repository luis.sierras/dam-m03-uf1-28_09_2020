package cat.itb.luissierras7e4.dam.m03.uf4.exercices.bicycles;


public class Model {
    String name;
    int gears;
    Marca brand;

    public Model(String name, int gears, Marca brand) {
        this.name = name;
        this.gears = gears;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Model{" +
                "name='" + name + '\'' +
                ", gears=" + gears +
                ", brand=" + brand +
                '}';
    }
}

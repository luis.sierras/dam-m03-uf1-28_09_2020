package cat.itb.luissierras7e4.dam.m03.uf4.exercices.TeamMotto;

public class VolleyTeam extends Team {
    String color;

    public VolleyTeam(String name, String motto, String color) {
        super(name, motto);
        this.color = color;
    }

    @Override
    public void shoutMotto() {
        System.out.println(motto + " " + color);
    }
}


package cat.itb.luissierras7e4.dam.m03.uf4.exercices;

public class StudentsApp {
    public static void main(String[] args) {
        StudentWithTextGrade student1 = new StudentWithTextGrade("Aitor", StudentWithTextGrade.Nota.EXCELLENT);
        StudentWithTextGrade student2 = new StudentWithTextGrade("Maria", StudentWithTextGrade.Nota.APROVAT);

        System.out.println(student1 + "\n" + student2);

    }
}

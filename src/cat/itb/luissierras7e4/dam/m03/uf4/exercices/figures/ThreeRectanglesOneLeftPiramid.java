package cat.itb.luissierras7e4.dam.m03.uf4.exercices.figures;

public class ThreeRectanglesOneLeftPiramid {
    public static void main(String[] args) {

        //ThreeRectanglesOneLeftPiramid

//rectangle: color: RED, llargada: 4, amplada: 5
//triangle: color: YELLOW, altura: 3
//rectangle color: GREEN, llargada: 3, amplada: 5

        RectangleFigure rectangleRED2= new RectangleFigure(ConsoleColors.RED,4,5);
        rectangleRED2.paint(System.out);
        LeftPiramidFigure piramidYELLOW= new LeftPiramidFigure(ConsoleColors.YELLOW,3);
        piramidYELLOW.paint(System.out);
        RectangleFigure rectangleGREEN2= new RectangleFigure(ConsoleColors.GREEN,3,5);
        rectangleGREEN2.paint(System.out);
    }
}

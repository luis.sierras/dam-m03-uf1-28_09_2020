package cat.itb.luissierras7e4.dam.m03.uf4.generalexam.gameenemies;

public abstract class Enemics {
    String name;
    int vida;

    public Enemics(String name, int vida) {
        this.name = name;
        this.vida = vida;
    }

    public abstract int attack(int force);
}

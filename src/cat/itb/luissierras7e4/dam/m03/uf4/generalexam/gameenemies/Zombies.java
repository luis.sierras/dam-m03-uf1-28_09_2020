package cat.itb.luissierras7e4.dam.m03.uf4.generalexam.gameenemies;

public class Zombies extends Enemics{
    String so;

    public Zombies(String name, int vida, String so) {
        super(name, vida);
        this.so = so;
    }

    @Override
    public int attack(int force){
        if(vida>0) {
            vida = vida - force;
        }

        return vida;

    }

}

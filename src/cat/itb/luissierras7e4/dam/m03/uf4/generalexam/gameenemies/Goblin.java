package cat.itb.luissierras7e4.dam.m03.uf4.generalexam.gameenemies;

public class Goblin extends Enemics{

    public Goblin(String name, int vida) {
        super(name, vida);
    }

    @Override
    public int attack(int force){
        if(vida>0) {
            vida = vida - force;
        }

        return vida;

    }

}

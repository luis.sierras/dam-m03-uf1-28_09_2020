package cat.itb.luissierras7e4.dam.m03.uf4.generalexam.twitter;

import java.util.List;
//NO CONSIGUO QUE ME SALGAN LAS VOTACIONES, TAMBIEN HE INTENTADO HACER LAS OPCIONES CON UNA LISTA PERO NO LO HE LOGRADO
//PROBLEMAS PRIMER MUNDISTA

public class PollTweets extends Tweets{
    String opcio1;
    String opcio2;
    String opcio3;
    String opcio4;
    int vota1;
    int vota2;
    int vota3;
    int vota4;
    int vots;


    public PollTweets(String user, String data, String text, String opcio1, String opcio2, String opcio3, String opcio4) {
        super(user, data, text);
        this.opcio1 = opcio1;
        this.opcio2 = opcio2;
        this.opcio3 = opcio3;
        this.opcio4 = opcio4;
    }


    public void vote(int index){
        if(index == 0){
            vota1=+1;
            vots =+1;
        } else if (index == 1 ){
            vota2=+1;
            vots =+1;
        }else if (index == 2 ){
            vota3=+1;
            vots =+1;
        }else if (index == 3 ) {
            vota4 = +1;
            vots = +1;
        }

    }

    @Override
    public void printTweet(){
        System.out.println("@"+user +" · "+ data +
                            "\n" + text +
                "\n- ("+vota1+"/"+vots+")" +opcio1+
                "\n- ("+vota2+"/"+vots+")" +opcio2+
                "\n- ("+vota3+"/"+vots+")" +opcio3+
                "\n- ("+vota4+"/"+vots+")" +opcio4);

        System.out.println();
    }
}

package cat.itb.luissierras7e4.dam.m03.uf4.generalexam.twitter;

public class Tweets {
    String user;
    String data;
    String text;

    public Tweets(String user, String data, String text) {
        this.user = user;
        this.data = data;
        this.text = text;
    }


    public void printTweet(){
        System.out.println("@"+user +" · "+ data);
        System.out.println(text);
        System.out.println();
    }
}

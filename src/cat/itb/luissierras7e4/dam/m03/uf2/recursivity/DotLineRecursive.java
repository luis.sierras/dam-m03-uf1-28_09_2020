package cat.itb.luissierras7e4.dam.m03.uf2.recursivity;

import java.util.Scanner;

public class DotLineRecursive {
    public static String dots(int n){
        if(n ==0 ) {
            return "";
        }
        else{
            return "."+ dots(n-1);
        }
    }

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        System.out.println(dots(n));
    }
}

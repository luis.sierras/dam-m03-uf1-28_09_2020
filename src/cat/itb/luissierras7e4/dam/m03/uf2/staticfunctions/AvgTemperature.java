package cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class AvgTemperature {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);

//AvgTemperature
        System.out.println("Ha fet " + IntegerLists.mitjana(list) +" graus de mitjana");
    }
}

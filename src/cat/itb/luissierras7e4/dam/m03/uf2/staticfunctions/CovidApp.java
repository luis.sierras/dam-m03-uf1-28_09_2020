package cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class CovidApp {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        List <Integer> dailyCases=CovidCalculations.readDailyCasesFromScanner(scanner);
        double growth = CovidCalculations.growthRates(dailyCases).get(CovidCalculations.growthRates(dailyCases).size()-1);
        System.out.printf("Hi ha hagut %d casos en total, amb una mitjana de %.2f per dia. " +
                        "L'útlim creixement és de %.2f",
                CovidCalculations.countTotalCases(dailyCases), CovidCalculations.average(dailyCases), growth);
    }
}

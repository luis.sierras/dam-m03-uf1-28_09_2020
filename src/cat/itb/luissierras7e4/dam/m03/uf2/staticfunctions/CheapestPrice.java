package cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class CheapestPrice {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);

//CheapestPrice
        System.out.println("El producte més econòmic val: " + IntegerLists.min(list) +"€");
    }
}

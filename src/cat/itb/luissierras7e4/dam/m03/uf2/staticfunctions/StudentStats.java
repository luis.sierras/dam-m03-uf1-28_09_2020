package cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class StudentStats {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);

//StudentStats
        System.out.println("Nota mínima: " + IntegerLists.min(list));
        System.out.println("Nota màxima: " + IntegerLists.max(list));
        System.out.println("Nota mitjana: " + IntegerLists.mitjana(list));
    }
}

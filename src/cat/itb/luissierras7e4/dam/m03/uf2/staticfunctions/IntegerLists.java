package cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerLists {

    //IntegerListsReaderSample
    public static List<Integer> readIntegerList(Scanner scanner){
        int number =scanner.nextInt();
        List<Integer> list = new ArrayList<Integer>();

        while(number!=-1) {
            list.add(number);
            number =scanner.nextInt();
        }
        return list;
    }

    //CheapestPrice
    public static int min(List<Integer> list){

        int newValor= list.get(0);
        for (Integer integer : list) {
            if (integer < newValor) {
                newValor = integer;
            }
        }
        return newValor;
    }

    //OldestStudent
    public static int max(List<Integer> list){

        int newValor= list.get(0);
        for (Integer integer : list) {
            if (integer > newValor) {
                newValor = integer;
            }
        }
        return newValor;
    }

    //AvgTemperature
    public static double mitjana(List<Integer> list){
        double suma = 0;
        for (int value:list){
            suma += value;
        }
        return suma / list.size();

    }

    public static int readDistantAge(List<Integer> ages) {
        return (max(ages)-min(ages));
    }

    public static int sum(List<Integer> people) {
        int total=0;
        for (int p:people){
            total+=p;
        }
        return total;
    }
}

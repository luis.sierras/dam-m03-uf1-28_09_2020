package cat.itb.luissierras7e4.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FromAvgSallaryInfo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Employee> employees = readEmployees(scanner);
        List<Employee> underAverage = getUnderAverage(employees);

        printEmployee(underAverage);
    }

    private static void printEmployee(List<Employee> employees) {
        for(Employee employee:employees){
            System.out.println(employee.getName());
        }
    }


    private static List<Employee> getUnderAverage(List<Employee> employees) {
        double averageSalary = calculateAverageSalary(employees);
        return filterLowerSalary(employees, averageSalary);
    }

    private static List<Employee> filterLowerSalary(List<Employee> employees, double averageSalary) {
        List<Employee> list = new ArrayList<>();
        for(Employee employee: employees){
            if(employee.getSalary()<averageSalary){
                list.add(employee);
            }
        }
        return list;
    }

    private static double calculateAverageSalary(List<Employee> employees) {
        double sum = 0;
        for(Employee employee : employees){
            sum+=employee.getSalary();
        }
        return sum/employees.size();
    }

    private static List<Employee> readEmployees(Scanner scanner) {
        List<Employee> employees = new ArrayList<>();
        int size = scanner.nextInt();
        for(int i=0; i<size; ++i){
            Employee employee = readEmployee(scanner);
            employees.add(employee);
        }
        return employees;
    }

    private static Employee readEmployee(Scanner scanner) {
        double salary = scanner.nextDouble();
        String name = scanner.nextLine();
        return new Employee(salary, name);
    }
}

package cat.itb.luissierras7e4.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GradeCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<StudentGrades> studentGrades = readStudentGrades(scanner);
        printAverageGrade(studentGrades);

    }

    public static void printAverageGrade(List<StudentGrades> studentsGrades) {
        for(StudentGrades studentGrades : studentsGrades){
            printAverageGrade(studentGrades);
        }
    }

    public static void printAverageGrade(StudentGrades studentGrades) {
        String name = studentGrades.getName();
        double grade = studentGrades.getAverageGrade();//calculateAverageGrade(studentGrades);
        System.out.printf("%s: %.1f%n", name, grade);
    }

    public static double calculateAverageGrade(StudentGrades studentGrades) {
        // nota = nota_exercicis * 0.3 + nota_examen * 0.3 + nota_projecte * 0.4
        return studentGrades.getNotaExercicis()*0.3 + studentGrades.getNotaExamen()*0.3 + studentGrades.getNotaProjecte()*0.4;
    }


    public static List<StudentGrades> readStudentGrades(Scanner scanner) {
        int size = scanner.nextInt();
        List<StudentGrades> result = new ArrayList<>();
        for(int i=0; i<size; ++i){
            StudentGrades grades = readStudent(scanner);
            result.add(grades);
        }
        return result;
    }

    public static StudentGrades readStudent(Scanner scanner) {
        String name = scanner.next();
        double notaExercicis = scanner.nextDouble();
        double notaExamen = scanner.nextDouble();
        double notaProjecte = scanner.nextDouble();
        return new StudentGrades(name, notaExercicis, notaExamen,notaProjecte);
    }
}

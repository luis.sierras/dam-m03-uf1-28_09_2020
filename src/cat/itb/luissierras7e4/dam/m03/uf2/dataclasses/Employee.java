package cat.itb.luissierras7e4.dam.m03.uf2.dataclasses;

public class Employee {
    double salary;
    String name;

    public Employee(double salary, String name) {
        this.salary = salary;
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }
}

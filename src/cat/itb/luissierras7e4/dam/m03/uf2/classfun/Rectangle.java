package cat.itb.luissierras7e4.dam.m03.uf2.classfun;

public class Rectangle {
    int id;
    double width;
    double height;

    public Rectangle(int id, double width, double height) {
        this.id = id;
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getArea() {
        return height*width;
    }

}



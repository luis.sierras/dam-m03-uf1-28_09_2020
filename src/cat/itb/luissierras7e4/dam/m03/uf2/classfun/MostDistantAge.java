package cat.itb.luissierras7e4.dam.m03.uf2.classfun;

import cat.itb.luissierras7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MostDistantAge {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        List<Integer> ages = IntegerLists.readIntegerList(scanner);

        System.out.print(IntegerLists.readDistantAge(ages));

    }

}

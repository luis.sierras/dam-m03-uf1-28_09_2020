package cat.itb.luissierras7e4.dam.m03.uf2.practica;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HighPeaks {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Peaks> peaks = readPeaks(scanner);
        printPeaks(peaks);
    }

    /**
     * Reads a new List of Peaks
     * @param scanner list
     * @return List Peaks
     */
    private static List<Peaks> readPeaks(Scanner scanner) {
        List<Peaks> peaks = new ArrayList<>();
        int size = scanner.nextInt();
        for (int i=0; i<size; i++){
            Peaks peak = Peaks.readPeack(scanner);
            peaks.add(peak);
        }
        return peaks;
    }

    private static void printPeaks(List<Peaks> peaks) {
        System.out.println("------------------------");
        System.out.println("--- Cims aconseguits ---");
        System.out.println("------------------------");
        printPeak(peaks);
        System.out.println("------------------------");
        System.out.printf("N. cims: %d%n", peaks.size());
        Peaks.getHigherPeak(peaks);
        Peaks.getLowerTimePeak(peaks);
        Peaks.getFasterPeak(peaks);
        System.out.println("------------------------");
    }

    private static void printPeak(List<Peaks> peaks) {
        for (Peaks peak : peaks){
            System.out.println(peak);
        }
    }
}
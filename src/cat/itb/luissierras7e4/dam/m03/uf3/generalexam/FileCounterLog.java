package cat.itb.luissierras7e4.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class FileCounterLog {
    public static void main(String[] args) throws IOException {

/*Volem fer un petit programa que ens generi un log de quants fitxers tenen diferents carpetes. */

        //ruta home y fecha actual
        Scanner scanner=new Scanner(System.in);
        String homePath = System.getProperty("user.home");
        String date = LocalDateTime.now().toString();

        //ruta para revisar y convertir en path
        System.out.println("ruta:");
        String rutaString = scanner.nextLine();
        Path ruta = Path.of(rutaString);

        //contador de archivos, obtener ruta counterlog, crear si no existe y añadir al final si existe
        //con printStream fecha actual, numero de ficheros y ruta.
        int count=countFiles(ruta);
        Path path = Path.of(homePath,"counterlog.txt");
        try (OutputStream outputStream = Files.newOutputStream(path, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            PrintStream printStream = new PrintStream(outputStream, true);
            printStream.printf("\n"+ date+" - Tens %d fitxers a %s",count,ruta);
        }



    }

    private static int countFiles(Path ruta) throws IOException {
        Stream<Path> fileStream = Files.list(ruta);
        List<Path> files = fileStream.collect(Collectors.toList());
        int count=0;
        for (Path file:files){
            count++;

        }
        return count;
    }
}

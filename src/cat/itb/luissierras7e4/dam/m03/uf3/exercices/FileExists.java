package cat.itb.luissierras7e4.dam.m03.uf3.exercices;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class FileExists {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        Path path = Path.of(scanner.next());
        boolean exists = Files.exists(path);

        System.out.print(exists);

    }
}

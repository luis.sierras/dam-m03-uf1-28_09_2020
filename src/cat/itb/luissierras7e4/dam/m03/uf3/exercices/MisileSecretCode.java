package cat.itb.luissierras7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class MisileSecretCode {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Path fitxer = Files.createTempFile("secret", ".txt");
        Files.writeString(fitxer, scanner.next());
        boolean exists = Files.exists(fitxer);

        if(exists){
            System.out.println("missil preparat");
        }
    }
}
